package sda3.amen.com.butterknifeexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.line1)
    protected TextView line1;

    @BindView(R.id.line2)
    protected TextView line2;

    @BindView(R.id.button1)
    protected Button button1;

    @BindView(R.id.button2)
    protected Button button2;

    @BindView(R.id.button3)
    protected Button button3;


    @OnClick({R.id.button1, R.id.button2, R.id.button3})
    protected void onButtonClickMethod(View v) {
        if (v instanceof Button) {
            Button buttonClicked = (Button) v;
            line1.setText("Linia pierwsza zmienia tekst. " + buttonClicked.getText());
//            line1.setText("Linia pierwsza zmienia tekst. " + ((Button)v).getText());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        View.OnClickListener listener  =new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                line1.setText("Linia pierwsza zmienia tekst.");
//            }
//        };
//
//        button1.setOnClickListener(listener);
//        button2.setOnClickListener(listener);
//        button3.setOnClickListener(listener);
    }
}
