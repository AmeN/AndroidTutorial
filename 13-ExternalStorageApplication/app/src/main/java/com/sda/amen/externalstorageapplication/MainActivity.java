package com.sda.amen.externalstorageapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.input);
    }

    public void clickWrite(View v) {
        String textToWrite = input.getText().toString();

        try {
            OutputStreamWriter writer = new OutputStreamWriter(getApplicationContext().openFileOutput("plik.txt", Context.MODE_PRIVATE));
            writer.write(textToWrite);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clickRead(View v) {
        String readString = "";

        try {
            InputStreamReader reader = new InputStreamReader(getApplicationContext().openFileInput("plik.txt"));
            BufferedReader buf_reader = new BufferedReader(reader);
            readString = buf_reader.readLine();
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        input.setText(readString);
    }
}
