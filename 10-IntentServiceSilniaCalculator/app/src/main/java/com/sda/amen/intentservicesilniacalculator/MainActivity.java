package com.sda.amen.intentservicesilniacalculator;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView etykieta;
    private EditText pole_tekstowe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etykieta = (TextView) findViewById(R.id.textView);
        pole_tekstowe = (EditText) findViewById(R.id.editText);
    }

    public void wykonaj_obliczenia(View v) {
        int przekazana_liczba = Integer.parseInt(pole_tekstowe.getText().toString());

        Intent i = new Intent(getApplicationContext(), UslugaObliczeniowa.class);
        i.putExtra("liczba", przekazana_liczba);
        startService(i);
    }

    public static class UslugaObliczeniowa extends IntentService {

        public UslugaObliczeniowa() {
            super("Usluga obliczeniowa");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            if (intent != null) {
                if (intent.hasExtra("liczba")) {
                    int liczba = intent.getIntExtra("liczba", 0);

                    long wynik = 1L;
                    for (int i = 1; i < liczba; i++) {
                        wynik += i;
                    }

                    final long ostatecznyWynik = wynik;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), String.valueOf(ostatecznyWynik), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    }
}
