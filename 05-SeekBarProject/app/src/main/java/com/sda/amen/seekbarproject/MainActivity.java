package com.sda.amen.seekbarproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private SeekBar pasek;
    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pasek = (SeekBar) findViewById(R.id.seekBar);
        label = (TextView) findViewById(R.id.textView);

        pasek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // czynność która wykona się pod wpływem zmiany wartości suwaka
                label.setText(String.valueOf(i));
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // czynność która wykona się pod wpływem rozpoczęcia przesunięcia suwaka
            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // czynność która wykona się pod wpływem zakończenia przesunięcia suwaka
            }
        });

    }
}
