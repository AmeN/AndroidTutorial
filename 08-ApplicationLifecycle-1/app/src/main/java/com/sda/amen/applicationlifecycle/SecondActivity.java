package com.sda.amen.applicationlifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        label = (TextView) findViewById(R.id.textView);

        if(getIntent().hasExtra("Parametr")){
            label.setText(getIntent().getStringExtra("Parametr"));
        }
    }

    @Override
    protected void onResume() {
        Toast.makeText(getApplicationContext(), "onResume SecondActivity", Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    protected void onPause() {
        Toast.makeText(getApplicationContext(), "onPause SecondActivity", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    public void killActivity(View v){
        finish();
    }
}
