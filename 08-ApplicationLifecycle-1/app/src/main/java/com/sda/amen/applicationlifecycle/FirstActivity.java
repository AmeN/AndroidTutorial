package com.sda.amen.applicationlifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {

    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        input = (EditText) findViewById(R.id.editText);
    }

    public void wykonaj_metode(View v) {

        // utworzenie intentu
        Intent i = new Intent(getApplicationContext(), SecondActivity.class);

        // dodanie dodatkowego parametru
        i.putExtra("Parametr", input.getText().toString());

        // uruchomienie activity (z intentu)
        startActivity(i);
    }


    @Override
    protected void onResume() {
        Toast.makeText(getApplicationContext(), "onResume FirstActivity", Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    protected void onPause() {
        Toast.makeText(getApplicationContext(), "onPause FirstActivity", Toast.LENGTH_SHORT).show();
        super.onPause();
    }
}
