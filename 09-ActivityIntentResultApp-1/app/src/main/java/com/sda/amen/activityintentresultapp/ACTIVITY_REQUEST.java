package com.sda.amen.activityintentresultapp;

/**
 * Created by amen on 4/19/17.
 */

public enum ACTIVITY_REQUEST {

    BUTTON_1_REQUEST(1),
    BUTTON_2_REQUEST(2);
    private int id;
    ACTIVITY_REQUEST(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }
}
