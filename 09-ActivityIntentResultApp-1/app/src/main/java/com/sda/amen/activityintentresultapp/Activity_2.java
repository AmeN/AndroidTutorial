package com.sda.amen.activityintentresultapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Activity_2 extends AppCompatActivity {

    private EditText poleTekstowe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        poleTekstowe = (EditText) findViewById(R.id.editText);
    }

    public void activity_2_click(View v){
        Intent i = new Intent();
        i.putExtra("tekst", poleTekstowe.getText().toString());
        setResult(RESULT_OK, i);
        finish();
    }
}
