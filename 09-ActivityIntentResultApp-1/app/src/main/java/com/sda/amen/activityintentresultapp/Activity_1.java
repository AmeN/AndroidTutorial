package com.sda.amen.activityintentresultapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Activity_1 extends AppCompatActivity {

    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        label = (TextView) findViewById(R.id.textView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_REQUEST.BUTTON_1_REQUEST.getId()) {
            if (data != null && data.hasExtra("tekst")) {
                label.setText("Button1 " + data.getStringExtra("tekst"));
            }
        } else {
            if (data != null && data.hasExtra("tekst")) {
                label.setText("Button2 " + data.getStringExtra("tekst"));
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void activity_1_click(View v) {
        Intent i = new Intent(getApplicationContext(), Activity_2.class);
        startActivityForResult(i, ACTIVITY_REQUEST.BUTTON_1_REQUEST.getId());
    }

    public void activity_1_click2(View v) {
        Intent i = new Intent(getApplicationContext(), Activity_2.class);
        startActivityForResult(i, ACTIVITY_REQUEST.BUTTON_2_REQUEST.getId());
    }
}
