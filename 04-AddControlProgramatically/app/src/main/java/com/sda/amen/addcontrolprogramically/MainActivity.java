package com.sda.amen.addcontrolprogramically;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout relative;
    private LinearLayout linear;

//    private int licznik_klikniec =0; // można tak
    private int licznik_klikniec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        licznik_klikniec = 0; // dobra praktyka - on create działa jako konstruktor

        relative = (RelativeLayout) findViewById(R.id.activity_main);   // główny layout aplikacji
        linear = (LinearLayout) findViewById(R.id.szukany_layout);      // layout pod buttonem
    }

    public void dodajKontrolke(View v) {
        final TextView dodawanaKontrolka = new TextView(linear.getContext());
        dodawanaKontrolka.setText("SDA jest spoko");

        dodawanaKontrolka.setOnClickListener(new View.OnClickListener() {
            int licznik = 0;

            @Override
            public void onClick(View v) {
                dodawanaKontrolka.setText("Kontrolka została kliknięta " + (++licznik));
            }
        });

        linear.addView(dodawanaKontrolka);
    }

    /**
     * Dodawanie kontrolki na Relative Layout
     */
//    public void dodajKontrolke(View v) {
//        TextView dodawanaKontrolka = new TextView(this);
//        RelativeLayout.LayoutParams parametry = new RelativeLayout.LayoutParams(200, 100);
//        parametry.topMargin = 200+ 100 * licznik_klikniec;
//
//        dodawanaKontrolka.setLayoutParams(parametry);
//        dodawanaKontrolka.setText("SDA jest spoko");
//
//        relative.addView(dodawanaKontrolka);
//        licznik_klikniec++;
//
//    }
}
