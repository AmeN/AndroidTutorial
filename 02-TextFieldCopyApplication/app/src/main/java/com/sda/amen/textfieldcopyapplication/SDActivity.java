package com.sda.amen.textfieldcopyapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SDActivity extends AppCompatActivity {

    private EditText input_variable;
    private TextView label_variable;
    private Button button_variable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);


        input_variable = (EditText) findViewById(R.id.input_field);
        label_variable = (TextView ) findViewById(R.id.output_label);
        button_variable = (Button) findViewById(R.id.clickable_button);

        button_variable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                label_variable.setText(input_variable.getText());
            }
        });
    }

//    public void definedMethod(View v){
//        label_variable.setText(input_variable.getText());
//    }
}
