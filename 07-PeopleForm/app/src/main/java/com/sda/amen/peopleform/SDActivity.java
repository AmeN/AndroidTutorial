package com.sda.amen.peopleform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SDActivity extends AppCompatActivity {
    private EditText input_imie;
    private EditText input_nazwisko;
    private DatePicker input_date;
    private TextView label_wiek;
    private CheckBox check_czy_kawaler;
    private Spinner spinner_plec;
    private EditText input_malzonek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        // przypisanie kontrolek
        input_imie = (EditText) findViewById(R.id.input_imie);
        input_nazwisko = (EditText) findViewById(R.id.input_nazwisko);
        input_date = (DatePicker) findViewById(R.id.input_date);
        check_czy_kawaler = (CheckBox) findViewById(R.id.check_state);
        spinner_plec = (Spinner) findViewById(R.id.spinner_plec);
        input_malzonek = (EditText) findViewById(R.id.input_malzonek);

        Log.d(getClass().getName(), "Wystartowałem aplikację.");

        // dodanie listnera zmiany daty
        input_date.init(1992, 9, 25, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                // wyświetlenie komunikatu
                Toast toast = Toast.makeText(getApplicationContext(), "Date changed" + i + " " + i1 + " " + i2, Toast.LENGTH_LONG);
                toast.show();
            }
        });

        // ustawienie czynności zaznaczenia i odznaczenia checkbox'a
        input_malzonek.setEnabled(false);
        check_czy_kawaler.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                input_malzonek.setEnabled(!b);
            }
        });

        // dodanie płci do spinnera (listy wyboru)
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getApplicationContext(),
                R.array.plcie,  // patrz do resources (res/values/strings)
                android.R.layout.simple_spinner_dropdown_item);
        spinner_plec.setAdapter(adapter);

        // dodanie czynności pod wpływem zmiany płci
        spinner_plec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = adapter.getItem(i).toString();
                if(selected.equals("Mężczyzna")){
                    check_czy_kawaler.setText("Kawaler");
                }else{
                    check_czy_kawaler.setText("Panna");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


}
