package com.sda.amen.spinnerzgadywanka;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class SDActivity extends AppCompatActivity {

    private TextView label;
    private Spinner spinner;

    // do przechowywania
    private int wylosowanaLiczba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        label = (TextView) findViewById(R.id.textView);
        spinner = (Spinner) findViewById(R.id.spinner);

        // ### Generowanie liczby
        Random generator = new Random();
        wylosowanaLiczba = generator.nextInt(11);
        // ###

        // #################################
        // ### Dodanie elementów do spinnera - opcja 1
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            list.add(String.valueOf(i));
        }
        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(
                getApplicationContext(),                // KONTEKST APLIKACJI
                android.R.layout.simple_spinner_item,   // typ wyświetlania (wygląd) spinnera
                list);                                  // dane do wyświetlenia / wartości
        spinner.setAdapter(adapter);

        // #################################
        // ### Dodanie elementów do spinnera - opcja 2
        // #################################
//            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
//                    getApplicationContext(),      // KONTEKST APLIKACJI
//                    R.array.spinner_values,       // WARTOŚCI (katalog res/values/strings - tam znajduje się string-array)
//                    android.R.layout.simple_spinner_item); // typ (wygląd) spinnera
//            spinner.setAdapter(adapter);
        // ###
        // #################################


        // ### Dodanie czynności która ma się wykonać pod wpływem zmiany wybranej wartości z kontrolki
        // pod wpływem zmiany wyboru spinnera wykona się czynność onItemSelected
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int numer_el_ze_spinnera, long l) {
                int sparsowanaLiczba = Integer.parseInt(adapter.getItem(numer_el_ze_spinnera));
                if (sparsowanaLiczba == wylosowanaLiczba) {
                    label.setTextColor(Color.GREEN);
                    label.setText("wygrałeś!");
                } else {
                    label.setTextColor(Color.RED);
                    label.setText("zła liczba");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
