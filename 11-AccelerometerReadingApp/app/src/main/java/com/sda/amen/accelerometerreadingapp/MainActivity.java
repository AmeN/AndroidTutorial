package com.sda.amen.accelerometerreadingapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView axis_x_label;
    private TextView axis_y_label;
    private TextView axis_z_label;

    private SensorManager sensorManager;
    private Sensor gyroscope;
    private Sensor accelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        axis_x_label = (TextView) findViewById(R.id.axis_x);
        axis_y_label = (TextView) findViewById(R.id.axis_y);
        axis_z_label = (TextView) findViewById(R.id.axis_z);

        Object defaultSensor = getSystemService(Context.SENSOR_SERVICE);
        if(defaultSensor instanceof SensorManager) {
            // pobranie usługi systemowej - usługi czujników/sensors
            sensorManager = (SensorManager) defaultSensor;

            // sprawdzam czy usługa posiada sensor - accelerometer
            if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {

                // jeśli posiadamy sensor, przypisujemy go do zmiennej
                accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
                sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_GAME);
            }
        }
        Toast.makeText(getApplicationContext(), "Powiadomienie o uruchomieniu aplikacji", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float value_x = sensorEvent.values[0];
            String string_value_x = String.valueOf(value_x);
            axis_x_label.setText(string_value_x);

            float value_y = sensorEvent.values[1];
            String string_value_y = String.valueOf(value_y);
            axis_y_label.setText(string_value_y);

            float value_z = sensorEvent.values[2];
            String string_value_z = String.valueOf(value_z);
            axis_z_label.setText(string_value_z);
        }else if(sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE){
            //
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
