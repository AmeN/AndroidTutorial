package com.sda.amen.twolabelsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SDActivity extends AppCompatActivity {

    private TextView label_one;
    private TextView label_two;

    private Button clickable_button;

    private int click_counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        label_one = (TextView ) findViewById(R.id.label_first);
        label_two = (TextView ) findViewById(R.id.label_second);

        clickable_button = (Button) findViewById(R.id.our_button);
    }

    public void buttonClickMethod(View v){
        // if no clicks yet
        if(click_counter == 0){
            label_one.setText("Text for label number 1");
        }else if(click_counter == 1){ // after 1 click
            label_two.setText("Other text on second click.");
        }
        // if button was clicked more than once, nothing will happen,
        // only click_counter incrementation

        // increase counter
        click_counter++;
    }
}
