package com.sda.amen.helloworldapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SDActivity extends AppCompatActivity {

    // will be connected to the label
    private TextView label;

    // will be connected to the button
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        button = (Button)findViewById(R.id.button);
        label = (TextView) findViewById(R.id.label_hello);
    }

    public void buttonClickAction(View v){
        label.setText("Hello World!");
    }
}
